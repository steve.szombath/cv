# István Szombath

I am **Test Architect**, **Chief Infrastructure Engineer**, and **DevOps** evangelist on a huge international HMI project at an international leading software company.

## Experience

### 2015–2022 — <small>Evosoft Kft.</small>

* **Test Architect**; responsible for test strategy, coach developers on testing, drive and improve quality feedback loop
* **Chief Infrastructure Engineer**; responsible for git migration on industry project(s), improve CI/CD pipelines, drive and coach CI/CD of teams and business parners, drive Inner Source code culture within the whole company
* **Trainer** of architect candidates and (new) employees on DevOps and Testing topics
> `JS`, `TypeScript`, `C#`, `Java`, `Python`, `shell`
> `.NET`, `React`, `SpringBoot`
> `git`, `git-workflows`, `git-lfs`, `AWS`, `IaC`, `Terraform`, `Docker`, `Linux` 
> `GitLab`, `Azure-DevOps`, `VMWare`, `Jenkins`, `CI/CD`, `JFrog Artifactory`, `Gradle`, `Packer`, `Inner Source`
> `xUnit`, `TDD`, `BDD`, `Gherkin`, `Cucumber`


### 2012–2015 — <small>OptXware K+F Kft.</small>

* Senior Java developer
* Release Manager; responsible for CI/CD pipeline and releases
> `AUTOSAR RTE`, `MISRA C`, `Java`, `Eclipse`, `EMF`, `Jenkins`, `Python`

### 2008–2012 — <small>Fault Tolerant Research Group (BME)</small>

* Cloud-Native Java Developer, Cloud Infrastructure Engineer, SysAdmin
> `Java`, `AWS`, `ESXi`, `Linux`, `scripting`, `Big Data` 

### 2008 — <small>IBM Zurich Research Laboratory</small>

* Developer on a Big Data project (network mining, service discovery)

## Education

* 2003–2008 — Budapest University of Technology and Economics - M.Sc. in Technical Informatics, spec. in Design of Information Infrastructure

## IT Skills <small>additional</small>

* EvoSummit 2022 talk on Inner Source
* Innovator of 2021 Award (git scaling)
* Siemens Certified **Test Architect** (~ISTQB Advanced Technical Test Analyst)
* **ISTQB** FL
* **ITIL** v3
* Networking
* Information security
* Secure coding
* Documentation: `LaTeX`, `markdown`, `mkdocs`, `wiki`
* Big Data (exploratory data mining, parallel coordinates, graph sampling, large graph visualization)

## Soft Skills

* English, German
* Presentation / Trainer / Coaching skills

## Hobby

* Triathlon (IM in 2021 14:01:47)
